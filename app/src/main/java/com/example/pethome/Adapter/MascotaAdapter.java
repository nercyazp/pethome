package com.example.pethome.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pethome.InfoMascotaActivity;
import com.example.pethome.R;
import com.example.pethome.modelos.Mascota;

import java.util.List;

public class MascotaAdapter extends RecyclerView.Adapter<MascotaAdapter.ViewHolder> {

    // se crea una lista
    private static List<Mascota> mascotaList;
    // se crea contructor para instanciar
    public MascotaAdapter(List<Mascota> mascotaList) {
        this.mascotaList = mascotaList;
    }

    @NonNull
    @Override
    public MascotaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // para pasar la card view comouna vista
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_mascotas,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MascotaAdapter.ViewHolder holder, int position) {
        holder.txtNombreMascota.setText(mascotaList.get(position).getNombre());
        holder.txtRazaMascota.setText(mascotaList.get(position).getRaza());
        holder.txtGeneroMascota.setText(mascotaList.get(position).getGenero());
        holder.txtDescripcion.setText(mascotaList.get(position).getDescripcion());
        //holder.imagenMascota.setImageResource(mascotaList.get(position).getIdImagen());
        //holder.position= holder.getAdapterPosition();
        //falta la logica para favoritos
    }
    //cuantos elementos de la lista-tamaño total
    @Override
    public int getItemCount() {
        return mascotaList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagenMascota;
        private TextView txtNombreMascota, txtRazaMascota, txtFechaMascota, txtGeneroMascota, txtDescripcion;
        private int position;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagenMascota = itemView.findViewById(R.id.imagenMascota);
            txtNombreMascota = itemView.findViewById(R.id.txtNombreMascota);
            txtRazaMascota = itemView.findViewById(R.id.txtRazaMascota);
            txtFechaMascota = itemView.findViewById(R.id.txtFechaMascota);
            txtGeneroMascota = itemView.findViewById(R.id.txtGeneroMascota);
            txtDescripcion = itemView.findViewById(R.id.txtDescripcion);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), InfoMascotaActivity.class);
                    intent.putExtra("nombre", mascotaList.get(position).getNombre());
                    intent.putExtra("raza", mascotaList.get(position).getRaza());
                    intent.putExtra("edad", mascotaList.get(position).getFecha());
                    intent.putExtra("sexo", mascotaList.get(position).getGenero());
                    intent.putExtra("descripcion", mascotaList.get(position).getDescripcion());
                    intent.putExtra("imagen", mascotaList.get(position).getIdImagen());
                    view.getContext().startActivity(intent);
                }
            });
        }
    }
}
