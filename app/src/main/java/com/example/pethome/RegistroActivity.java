package com.example.pethome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pethome.modelos.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class RegistroActivity extends AppCompatActivity {

    private EditText edNombrePersona, edTelefonoPersona, edDireccionPersona, edFechaNacimientoPersona, edCorreoRegistro, edContraseñaRegistro ;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        mAuth = FirebaseAuth.getInstance();
        //nos traemos la base de datos para saber en donde es que vamos a guardar el registro
        mDatabase = FirebaseDatabase.getInstance().getReference();

        edNombrePersona = findViewById(R.id.edNombrePersona);
        edTelefonoPersona = findViewById(R.id.edTelefonoPersona);
        edDireccionPersona = findViewById(R.id.edDireccionPersona);
        edFechaNacimientoPersona = findViewById(R.id.edFechaNacimientoPersona);
        edCorreoRegistro = findViewById(R.id.edCorreoRegistro);
        edContraseñaRegistro = findViewById(R.id.edContraseñaRegistro);
    }

    public void registrarUsuario(View view){
        String nombre = edNombrePersona.getText().toString();
        String telefono = edTelefonoPersona.getText().toString();
        String direccion = edDireccionPersona.getText().toString();
        String fechaNacimiento = edFechaNacimientoPersona.getText().toString();
        String email = edCorreoRegistro.getText().toString();
        String password = edContraseñaRegistro.getText().toString();




        if (email.isEmpty()){
            edCorreoRegistro.setError("El correo es requerido");
        }else if(password.isEmpty()){
            edContraseñaRegistro.setError("La contraseña es requerida");
        }else{
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //obtenemos mediante este metodo el usuario UID
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            String id =currentUser.getUid();
                            //se crea un hijo y su subhijo al que se le asigna el id osea el Uid
                            Usuario usuario = new Usuario(nombre, telefono, direccion, fechaNacimiento, email);
                            mDatabase.child("usuario").child(id).setValue(usuario);
                            // Ya se creo el usuario
                            FirebaseAuth.getInstance().signOut();
                            Toast.makeText(RegistroActivity.this, "Se registro correctamente", Toast.LENGTH_LONG).show();
                            Intent intent2 = new Intent(RegistroActivity.this, HomeActivity.class);
                            startActivity(intent2);
                        } else {
                            // No se creo el usuario envia msj
                            Toast.makeText(RegistroActivity.this, "No se creo el usuario", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
    }
}