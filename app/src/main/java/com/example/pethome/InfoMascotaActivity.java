package com.example.pethome;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoMascotaActivity extends AppCompatActivity {
    //aca los capturamos para procesar
    private ImageView imagenMascotaInfo;
    private TextView txtNombreMascotaInfo, txtRazaMascotaInfo, txtEdadMascotaInfo, txtSexoMascotaInfo, txtDescripcionMascotaInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_mascota);

        imagenMascotaInfo = findViewById(R.id.imagenMascotaInfo);
        txtNombreMascotaInfo = findViewById(R.id.txtNombreMascotaInfo);
        txtRazaMascotaInfo = findViewById(R.id.txtRazaMascotaInfo);
        txtEdadMascotaInfo = findViewById(R.id.txtEdadMascotaInfo);
        txtSexoMascotaInfo = findViewById(R.id.txtSexoMascotaInfo);
        txtDescripcionMascotaInfo = findViewById(R.id.txtDescripcionMascotaInfo);



        String nombre = getIntent().getStringExtra("nombre");
        String raza = getIntent().getStringExtra("raza");
        String edad = getIntent().getStringExtra("edad");
        int imagen = getIntent().getIntExtra("imagen",0);
        String sexo = getIntent().getStringExtra("sexo");
        String descripcion = getIntent().getStringExtra("descripcion");

        txtNombreMascotaInfo.setText(nombre);
        txtRazaMascotaInfo.setText(raza);
        txtEdadMascotaInfo.setText(edad);
        //imagenMascotaInfo.setImageResource(imagen);
        txtSexoMascotaInfo.setText(sexo);
        txtDescripcionMascotaInfo.setText(descripcion);

    }

    public void RegresarMascotas(View view){
        Util.intentActivity(this, HomeActivity.class);
    }

    public void adoptarMascota(View view){
        AlertDialog.Builder alerDialog = new AlertDialog.Builder(this)
                .setTitle("Adoptar")
                .setMessage("¿Seguro desea continuar con el proceso?")
                .setPositiveButton("Si, seguro", this::onClick)
                .setNegativeButton("cancelar",this::onClick);
        alerDialog.show();

    }

    private void onClick(DialogInterface dialogInterface, int i) {
        switch (i){
            case -1:
                whatsappIntent();
                break;
        }
    }

    public void whatsappIntent(){
        try {
            String numero ="573209630796";
            String mensaje ="Hola, deseas adoptar";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String uri = "whatsapp://send?phone=" + numero + "&text" + mensaje;
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        }catch (Exception e){
            System.err.println(e.toString());
            System.out.println("Debe instalar Whatsapp");
            //Toast.makeText(this, "Debe instalar WhatsApp en su Movil", Toast.LENGTH_LONG).show();
        }

    }
}