package com.example.pethome.controlador;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.pethome.fragmentos.GatosFragment;
import com.example.pethome.fragmentos.OtrosFragment;
import com.example.pethome.fragmentos.PerrosFragment;

public class PagerController extends FragmentPagerAdapter {

    private int numDeItemTab;

    public PagerController(@NonNull FragmentManager fm, int numDeItemTab) {
        super(fm, numDeItemTab);
        this.numDeItemTab = numDeItemTab;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return new PerrosFragment();
            case 1: return new GatosFragment();
            case 2: return new OtrosFragment();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return numDeItemTab;
    }
}
