package com.example.pethome;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class AdoptarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adoptar);
    }
    public void whatsappIntent(View view) {
        try {
            String numero = "573209630796";
            String mensaje = "Hola, deseas adoptar";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String uri = "whatsapp://send?phone=" + numero + "&text" + mensaje;
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        } catch (Exception e) {
            System.err.println(e.toString());
            System.out.println("Debe instalar Whatsapp");
            Toast.makeText(this, "Debe instalar WhatsApp en su Movil", Toast.LENGTH_LONG).show();
        }
    }
    public void RegresarMascotas(View view){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}