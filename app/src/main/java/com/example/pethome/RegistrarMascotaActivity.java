package com.example.pethome;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.example.pethome.modelos.Mascota;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;

public class RegistrarMascotaActivity extends AppCompatActivity {

    private ImageView imMascota;
    private EditText edNombreMascotaRegistro, edRazaMascotaRegistro, edFechaMascotaRegistro, edDescripcionMascota, edPorqueMascotaRegistro;
    private CheckBox chVacunas;
    private RadioButton rbMacho, rbHembra;
    private Button btnRegresar, btnRegistar;
    private Spinner spMascotas;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private StorageReference mStorage;

    private final int REQUEST_CODE_PERMISSIONS = 0;
    private final int REQUEST_CODE_CAMERA = 1;
    private final int REQUEST_CODE_GALERIA = 2;
    private String uriImagenStorage = null;
    private Uri uriImagenGaleria;
    private byte[] dataImagen;
    private byte auxilidarData = 0;

    //
    private String genero;
    private String tipoMascota;
    private String nombreMascota;
    private String razaMascota;
    private String fechaNacimientoMascota;
    private String descripcionMascota;
    private String porqueMascota;
    boolean vacunas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarmascota);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        imMascota = findViewById(R.id.imMascota);
        edNombreMascotaRegistro = findViewById(R.id.edNombreMascotaRegistro);
        edRazaMascotaRegistro = findViewById(R.id.edRazaMascotaRegistro);
        edFechaMascotaRegistro = findViewById(R.id.edFechaMascotaRegistro);
        edDescripcionMascota = findViewById(R.id.edDescripcionMascota);
        edPorqueMascotaRegistro = findViewById(R.id.edPorqueMascotaRegistro);
        rbMacho = findViewById(R.id.rbMachoP);
        rbHembra = findViewById(R.id.rbHembra);
        chVacunas = findViewById(R.id.chVacunas);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnRegistar = findViewById(R.id.btnRegistar);
        spMascotas = findViewById(R.id.spMascotas);

        //Configuracion de Spinner

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.tipoMascotas, android.R.layout.simple_spinner_dropdown_item);
        spMascotas.setAdapter(adapter);
        edFechaMascotaRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturarFechaCumpleaños();
            }
        });
    }

    private void capturarFechaCumpleaños() {
        Calendar calendar =  Calendar.getInstance();
        int year  = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                edFechaMascotaRegistro.setText(year+"/"+(month+1)+"/"+dayOfMonth);
            }
        }, year, month, dayOfMonth);
        dialog.show();
    }

    public void RegresarMascotas(View view){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
    //Metodo para la foto de la mascota

    public void enviarFormulario(View view){
        String nombreImagen;
        StorageReference reference;
        UploadTask uploadTask;
        tipoMascota = spMascotas.getSelectedItem().toString();
        nombreMascota = edNombreMascotaRegistro.getText().toString();
        razaMascota = edRazaMascotaRegistro.getText().toString();
        fechaNacimientoMascota = edFechaMascotaRegistro.getText().toString();
        descripcionMascota = edDescripcionMascota.getText().toString();
        porqueMascota = edPorqueMascotaRegistro.getText().toString();
        vacunas  = chVacunas.isChecked();
        if (rbMacho.isChecked()){
            genero = "Macho";
        }else{
            genero = "Hembra";
        }
        //para verificar los campos
        switch (auxilidarData){
            case REQUEST_CODE_CAMERA:
                nombreImagen = nombreImagen();
                reference = mStorage.child("imagenes").child("imagen");
                uploadTask = reference.putBytes(dataImagen);
                obtenerUriImagenStorage(uploadTask,reference);
                break;
            case REQUEST_CODE_GALERIA:
                nombreImagen = nombreImagen();
                reference = mStorage.child("imagenes").child("imagen");
                uploadTask = reference.putFile(uriImagenGaleria);
                obtenerUriImagenStorage(uploadTask,reference);
                break;
            default:
                uriImagenStorage=null;
                break;
        }

        }

    //verificar que los campos no esten vacios
    private boolean verificarCarmpos(String nombreMascota) {
        if (nombreMascota.isEmpty()){
            edNombreMascotaRegistro.setError("Campo requerido");
            return false;
        }else{
            return true;
        }
    }

    public void tomarFotoMascota(View view) {
        //preguntar si los permisos de camara estan activos
        boolean permisosCamara = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)== PermissionChecker.PERMISSION_GRANTED;
        boolean permisosGalaeria = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)==PermissionChecker.PERMISSION_GRANTED;

        if(permisosCamara && permisosGalaeria){
            AlertDialog.Builder alert = new AlertDialog.Builder(this)
                    .setTitle("Captura Imagen")
                    .setMessage("Selecciona un nedio para capturar la imagen")
                    .setNegativeButton("camara", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //intension para abrir la camara
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CODE_CAMERA);
                        }
                    })
                    .setPositiveButton("galeria", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //intension para abrir la galeria
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, REQUEST_CODE_GALERIA);
                        }
                    });
            alert.show();

        }else{
            //Solicitar losmpermisos
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSIONS);
        }
    }
    //Metodo para la captura de la foto que se toma
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && data!=null){

            switch (requestCode){
                case REQUEST_CODE_CAMERA:
                    //Cambio pq esta llegando en miniatura
                    Bundle extras= data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    imMascota.setImageBitmap(bitmap);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    dataImagen = baos.toByteArray();
                    auxilidarData = REQUEST_CODE_CAMERA;




                    break;
                case REQUEST_CODE_GALERIA:
                    uriImagenGaleria = data.getData();
                    imMascota.setImageURI(uriImagenGaleria);
                    auxilidarData = REQUEST_CODE_GALERIA;

                    break;
            }
        }
    }

    private void obtenerUriImagenStorage(UploadTask uploadTask, StorageReference reference){
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    Log.e("ErrorCargaImagen", task.getException().toString());
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return reference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri uriImagen = task.getResult();
                    uriImagenStorage = uriImagen.toString();
                    //se trae el usuario que registro a la mascota
                    FirebaseUser currentUser = mAuth.getCurrentUser();
                    String idUsuario =currentUser.getUid();
                    //logica para  base de datos
                    Mascota mascota = new Mascota(null, nombreMascota, razaMascota, fechaNacimientoMascota, null, descripcionMascota, porqueMascota,idUsuario,vacunas, uriImagenStorage);
                    //crear los nodos o hijos y los hermanos -- el push genera un hijo unico que nunca se va a repetir
                    mDatabase.child("mascotas").child(tipoMascota).child(genero).push().setValue(mascota);
                } else {
                    Log.e("ErrorUriImg", task.getException().toString());
                }
            }
        });
    }

    private String nombreImagen() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        String idUsuario =currentUser.getUid();
        Date date = new Date();
        return idUsuario+"-"+date;
    }
}