package com.example.pethome;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

public class Util {
    public static void intentActivity(Context context,Class clase){
        Intent intent = new Intent(context, clase);
        context.startActivity(intent);
    }

}
