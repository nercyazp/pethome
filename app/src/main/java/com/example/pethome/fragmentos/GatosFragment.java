package com.example.pethome.fragmentos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pethome.Adapter.MascotaAdapter;
import com.example.pethome.R;
import com.example.pethome.modelos.Mascota;

import java.util.LinkedList;
import java.util.List;

public class GatosFragment extends Fragment {

    private RecyclerView recyclerViewGatos;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gatos, container, false);
        recyclerViewGatos = view.findViewById(R.id.recyclerViewGatos);

        MascotaAdapter mascotaAdapter = new MascotaAdapter(obtenerMascotas());
        recyclerViewGatos.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewGatos.setAdapter(mascotaAdapter);

        return view;
    }
    // a futuro sea por medio de la DB
    private List<Mascota> obtenerMascotas(){
        LinkedList<Mascota> listaMascotas = new LinkedList<>();
        listaMascotas.add(new Mascota("Manu", "Angora", "2021-01-28", "Hembra","Comelona y tierna", R.drawable.gato1));
        listaMascotas.add(new Mascota("Marcus", "Toyger", "2022-09-13", "Macho","le gusta jugar con lana", R.drawable.gato2));
        listaMascotas.add(new Mascota("Mr. Gato", "Europeo", "2019-12-02", "Macho","Tranquilo y comelon", R.drawable.gato3));
        listaMascotas.add(new Mascota("Manu", "Angora", "2021-01-28", "Hembra","Comelona y tierna", R.drawable.gato1));
        listaMascotas.add(new Mascota("Marcus", "Toyger", "2022-09-13", "Macho","le gusta jugar con lana", R.drawable.gato2));
        listaMascotas.add(new Mascota("Mr. Gato", "Europeo", "2019-12-02", "Macho","Tranquilo y comelon", R.drawable.gato3));
        return listaMascotas;
    }

}