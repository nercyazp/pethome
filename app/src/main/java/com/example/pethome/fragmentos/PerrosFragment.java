package com.example.pethome.fragmentos;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.pethome.Adapter.MascotaAdapter;
import com.example.pethome.R;
import com.example.pethome.modelos.Mascota;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.SQLOutput;
import java.util.LinkedList;
import java.util.List;

public class PerrosFragment extends Fragment {
    private RecyclerView recyclerViewPerros;
    private RadioButton rbHembraP, rbMachoP;
    private Button btnBuscarP;


    private DatabaseReference mDatabaase;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perros, container, false);

        mDatabaase = FirebaseDatabase.getInstance().getReference();

        recyclerViewPerros = view.findViewById(R.id.recyclerViewPerros);
        rbHembraP = view.findViewById(R.id.rbHembraP);
        rbMachoP = view.findViewById(R.id.rbMachoP);
        btnBuscarP = view.findViewById(R.id.btnBuscarP);

        recyclerViewPerros.setLayoutManager(new LinearLayoutManager(getContext()));

        btnBuscarP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cargarCardView();
            }
        });

        return view;
    }

    private void cargarCardView() {
        LinkedList<Mascota> mascotasList = new LinkedList<>();
        LinkedList<String> keyMascotasList = new LinkedList<>();
        String genero;
        String mascotSelect;
        if (rbHembraP.isChecked()) genero = "Hembra";
        else genero="Macho";
        DatabaseReference reference = mDatabaase.child("mascotas").child("Perro").child(genero);
        //para obtener los valores pero de pocos registros
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue()==null)                    {
                        mascotasList.clear();
                        MascotaAdapter mascotaAdapter = new MascotaAdapter(mascotasList);
                        recyclerViewPerros.setAdapter(mascotaAdapter);
                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                mascotasList.add(snapshot.getValue(Mascota.class));
                keyMascotasList.add(snapshot.getKey());
                MascotaAdapter mascotaAdapter = new MascotaAdapter(mascotasList);
                recyclerViewPerros.setAdapter(mascotaAdapter);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                int posicion = keyMascotasList.indexOf(snapshot.getKey());
                mascotasList.set(posicion,snapshot.getValue(Mascota.class));
                MascotaAdapter mascotaAdapter = new MascotaAdapter(mascotasList);
                recyclerViewPerros.setAdapter(mascotaAdapter);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                int posicion = keyMascotasList.indexOf(snapshot.getKey());
                keyMascotasList.remove(posicion);
                mascotasList.remove(posicion);
                MascotaAdapter mascotaAdapter = new MascotaAdapter(mascotasList);
                recyclerViewPerros.setAdapter(mascotaAdapter);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}