package com.example.pethome.fragmentos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pethome.Adapter.MascotaAdapter;
import com.example.pethome.R;
import com.example.pethome.modelos.Mascota;

import java.util.LinkedList;
import java.util.List;

public class OtrosFragment extends Fragment {

    private RecyclerView recyclerViewOtros;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otros, container, false);
        recyclerViewOtros = view.findViewById(R.id.recyclerViewOtros);

        MascotaAdapter mascotaAdapter = new MascotaAdapter(obtenerMascotas());
        recyclerViewOtros.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewOtros.setAdapter(mascotaAdapter);

        return view;
    }
    // a futuro sea por medio de la DB
    private List<Mascota> obtenerMascotas(){
        LinkedList<Mascota> listaMascotas = new LinkedList<>();
        listaMascotas.add(new Mascota("Paco", "Loro", "2021-12-12", "Macho","Parlanchin", R.drawable.ave1));
        listaMascotas.add(new Mascota("Sami", "Toy", "2022-10-03", "hembra","Super tierno y suave", R.drawable.conejo1));
        listaMascotas.add(new Mascota("Lili", "Perico", "2021-08-08", "hembra","Muy buena compañia", R.drawable.ave2));
        listaMascotas.add(new Mascota("Orejitas", "Belier", "2022-03-02", "Macho","Es muy consentido", R.drawable.conejo2));
        return listaMascotas;
    }
}