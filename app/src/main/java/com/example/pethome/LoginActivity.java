package com.example.pethome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private EditText edCorreoLogin, edContraseñaLogin;
    private FirebaseAuth mAuth;
    private ProgressDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edCorreoLogin = findViewById(R.id.edCorreoLogin);
        edContraseñaLogin = findViewById(R.id.edContraseñaLogin);
        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("ES");
        mDialog = new ProgressDialog(this);
        }

    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            lanzarHome();
        }
    }

    public void iniciarSesion(View view) {

        String email = edCorreoLogin.getText().toString();
        String password = edContraseñaLogin.getText().toString();
        if (email.isEmpty()) {
            edCorreoLogin.setError("El correo es requerido");
        } else if (password.isEmpty()) {
            edContraseñaLogin.setError("El password es requerido");
        } else {
            mDialog.setMessage("Iniciando Sesion...");
            mDialog.setCanceledOnTouchOutside(true);
            mDialog.show();
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override

                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d("Inicio Sesion satisfactorio", "signInWithEmail:success");
                                lanzarHome();
                                mDialog.dismiss();
                            } else {
                                mDialog.dismiss();
                                Log.w("Inicio de Sesion fallido", "signInWithEmail:failure");
                                // camino si no es satisfactorio el inicio de sesion
                                Toast.makeText(LoginActivity.this, "login fallido, verificar credenciales", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }
    public void Registro(View view){
        Intent intent = new Intent(this, RegistroActivity.class);
        startActivity(intent);
     }
    public void lanzarHome(){
         Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
         startActivity(intent);
         finish();
     }

    public void olvidoContraseña(View view) {

        String email = edCorreoLogin.getText().toString();
        if (email.isEmpty()) {
            edCorreoLogin.setError("EL correo es requerido");
        } else {
            //implementacion del Dialog progress
            mDialog.setMessage("Estamos trabajando...");
            mDialog.setCanceledOnTouchOutside(true);
            mDialog.show();
            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        // se teermina el progess cuando sabemos que el proceso es satisfactorio
                        mDialog.dismiss();
                        //cuando es satisfactorio
                        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                        alert.setTitle("Restaurar contraseña")
                                .setMessage("se envio el correo con el link de restauración")
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    }
                                });
                        alert.show();
                    } else {
                        mDialog.dismiss();
                        //cuando fallo
                        Log.e("Error send email", task.getException().toString());
                        Toast.makeText(LoginActivity.this, "No se logro enviar el mail", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}