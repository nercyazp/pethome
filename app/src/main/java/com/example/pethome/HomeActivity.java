package com.example.pethome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.pethome.controlador.PagerController;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;


public class HomeActivity extends AppCompatActivity {
    private TabLayout tabLayoutHome;
    private ViewPager viewPagerHome;
    private PagerController pagerController;
    private ProgressDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mDialog = new ProgressDialog(this);
        tabLayoutHome = findViewById(R.id.tabLayoutHome);
        viewPagerHome = findViewById(R.id.viewPagerHome);
        pagerController = new PagerController(getSupportFragmentManager(),tabLayoutHome.getTabCount());
        //permitira mostrar los layouts
        viewPagerHome.setAdapter(pagerController);
        tabLayoutHome.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            //detecta cuando el usuario selecciona un tab item, es apturado por la variable del tab
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerHome.setCurrentItem(tab.getPosition());
                pagerController.notifyDataSetChanged();
            }

            @Override
            //Es cuando estaba seleccionado 1 pero se deselecciono
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            //se reselecciona
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //permite sincronizar el boton con la vista en el momento de pasar de una frag. a otro
        viewPagerHome.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutHome));
    }
    // comandos para que inicie en una especifica pestaña ya sea perros, gatos o otros
    @Override
    protected void onStart() {
        super.onStart();
        tabLayoutHome.getTabAt(0).select();
    }
    @Override
    //para mostrar el menu tipo hamburguesa
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    //cuando se selecciona alguna opcion del menú
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.itemInicio:
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
            case R.id.itemAdoptar:
                Intent intent1 = new Intent(this, AdoptarActivity.class);
                startActivity(intent1);
                break;
            case R.id.itemDarEnAdopcion:
                Intent intent2 = new Intent(this, RegistrarMascotaActivity.class);
                startActivity(intent2);
                break;
            case R.id.itemApoyo:
                Intent intent4 = new Intent(this, ApoyarActivity.class);
                startActivity(intent4);
                break;
            case R.id.itemCerrarSesion:
                mDialog.setMessage("Cerrando Sesión...");
                mDialog.setCanceledOnTouchOutside(true);
                mDialog.show();
                FirebaseAuth.getInstance().signOut();
                Intent intent3 = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent3);
                finish();
                break;
            case R.id.itemContacto:
                Intent intent5= new Intent(this, InformacionActivity.class);
                startActivity(intent5);
                break;
            case R.id.itemPerfil:
                Toast.makeText(this, "Clic en Perfil", Toast.LENGTH_LONG).show();
                break;
            case R.id.itemBuscar:
                Toast.makeText(HomeActivity.this, "Click en Buscar", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    // Este metodo se usa si se quiere programar algo cuando la aplciacion esta en segundo plano
    /*@Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "On Stop", Toast.LENGTH_LONG).show();
    }*/
}


