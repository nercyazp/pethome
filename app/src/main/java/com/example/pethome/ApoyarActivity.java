package com.example.pethome;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ApoyarActivity extends AppCompatActivity {
    private ProgressDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apoyar);
        mDialog = new ProgressDialog(this);
    }
    public void RegresarMascotas(View view){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
    public void Enviar(View view){
        //implementacion del Dialog progress
        mDialog.setMessage("Enviando...");
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
        //Toast
        Toast.makeText(this,"Pronto nos podremos en contacto con usted", Toast.LENGTH_LONG).show();
    }
}