package com.example.pethome.modelos;

import com.google.firebase.database.IgnoreExtraProperties;

// se agrega la etiqueta para evitar que falle por valores nulos
@IgnoreExtraProperties
public class Mascota {
    private String tipoMascota, nombre, raza, fecha, genero, descripcion, porque, idUsuario, uriImagen;
    private Boolean vacunas;
    private  Integer idImagen; //Todo eliminar mas adelante

    public Mascota() {
    }

    //uso para pruebas
    public Mascota(String nombre, String raza, String fecha, String genero, String descripcion, int idImagen) {
        this.nombre = nombre;
        this.fecha = fecha;
        this.raza = raza;
        this.genero = genero;
        this.descripcion = descripcion;
        this.idImagen = idImagen;
    }

    public Mascota(String tipoMascota, String nombre, String raza, String fecha, String genero, String descripcion, String porque, String idUsuario, Boolean vacunas, String uriImagen) {
        this.tipoMascota = tipoMascota;
        this.nombre = nombre;
        this.raza = raza;
        this.fecha = fecha;
        this.genero = genero;
        this.descripcion = descripcion;
        this.porque = porque;
        this.idUsuario = idUsuario;
        this.vacunas = vacunas;
        this.uriImagen = uriImagen;
    }

    @Override
    public String toString() {
        return "Mascota{" +
                "tipoMascota='" + tipoMascota + '\'' +
                ", nombre='" + nombre + '\'' +
                ", raza='" + raza + '\'' +
                ", fecha='" + fecha + '\'' +
                ", genero='" + genero + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", porque='" + porque + '\'' +
                ", idUsuario='" + idUsuario + '\'' +
                ", vacunas=" + vacunas +
                ", idImagen=" + idImagen +
                '}';
    }

    public String getTipoMascota() {
        return tipoMascota;
    }

    public void setTipoMascota(String tipoMascota) {
        this.tipoMascota = tipoMascota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPorque() {
        return porque;
    }

    public void setPorque(String porque) {
        this.porque = porque;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Boolean getVacunas() {
        return vacunas;
    }

    public void setVacunas(Boolean vacunas) {
        this.vacunas = vacunas;
    }

    public Integer getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(Integer idImagen) {
        this.idImagen = idImagen;
    }

    public String getUriImagen() {
        return uriImagen;
    }

    public void setUriImagen(String uriImagen) {
        this.uriImagen = uriImagen;
    }
}



