package com.example.pethome.modelos;

import com.google.firebase.database.IgnoreExtraProperties;

// se agrega la etiqueta para evitar que falle por valores nulos
@IgnoreExtraProperties
public class Usuario {
    private String nombre, telefono, direccion, fechaNacimiento, correo;

    public Usuario() {
    }
    public Usuario(String nombre, String telefono, String direccion, String fechaNacimiento, String correo) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.fechaNacimiento = fechaNacimiento;
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
